# Ch 0 - Introduction to the Language

## Java

Java is a general-purporse programming language created by [James Gosling](https://en.wikipedia.org/wiki/James_Gosling), Mike Sheridan, and [Patrick Naughton](https://en.wikipedia.org/wiki/Patrick_Naughton) in 1991 and was first released to the public by [Sun Microsystems] (https://www.britannica.com/topic/Sun-Microsystems-Inc) in 1996.

### Popularity

[PYPL - PopularitY of Programming Language](http://pypl.github.io/PYPL.html)

### Versions of the language

* This course will focus on and utilize **Java 8** 

    - 79% of developers and applications are running on **Java version 8**
    - Long Term Supported (LTS) version of Java

### Characteristics of Java

* **Strict**
    - Enforces a strong set of rules and throws errors if those expectations are not met

* **Staticly-typed**
    - Everything must be explicitly pre-defined before your software can run

* **Object-oriented**
    - Classes and objects are at the core of the language

    - Almost everything is an object and implemented using classes

    - Allows one to represent real-world things/entities, their attributes and behaviors in software

### Writing our first Java program

When learning a new programming language, the first program everyone writes is: **The Classic Hello World**

```
public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello World");
    }
}
```

Note: When saving the contents above in a file, remember that the name of the file must match the name of the class as stated above. For example, the name of the file that would contain the java program above would be: HelloWorld.java

### Running our first Java program

Step 1: Complie

`javac HelloWorld.java`

Step 2: Run

`java HelloWorld`
