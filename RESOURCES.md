# Readings

1. [Difference between a Compiled and an Interpreted Language](https://kb.iu.edu/d/agsz)

2. [Programming Paradigms](https://cs.lmu.edu/~ray/notes/paradigms/)

3. [Programming Language and Different Types](https://www.typesnuses.com/types-of-programming-languages-with-differences/)

4. [Java Compilation Process](https://pdfs.semanticscholar.org/7d99/08e39b1dd1cd404107c5fc96666a6a1bd3c8.pdf)
