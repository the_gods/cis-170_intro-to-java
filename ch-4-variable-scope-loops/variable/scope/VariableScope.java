/*
    Examples demoing variable scope
*/
package variable.scope;

public class VariableScope {

    public static void main(String[] args) {
        { // Scope 1
            int a = 1;
            System.out.println("Scope 1: " + a);

            { // Scope 2
                int b = 2;
                System.out.println("Scope 2: " + a);

                System.out.println("Scope 2: " + b);

                { // Scope 3
                    int c = 3;
                    System.out.println("Scope 3: " + a);

                    System.out.println("Scope 3: " + b);

                    System.out.println("Scope 3: " + c);

                    { // Scope 4
                        int d = 4;
                        System.out.println("Scope 4: " + a);

                        System.out.println("Scope 4: " + b);
                      
                        System.out.println("Scope 4: " + c);

                        System.out.println("Scope 4: " + d);
                    }
                }
            }
        }
    }
}
