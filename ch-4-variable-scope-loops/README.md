# Ch 3 - Variable Scope and Loops

* [Variable Scope](#variable-scope)
* [Loops](#loops)

## Variable Scope

### Scope

Scope defines the life of your variable throughout your entire program or application. It represents in which blocks of code a variable is available (alive) and can be used.

Curly braces {} allow for the grouping of one or more related pieces of code/instructions known as a **block**.

There are two types of scope which exist for variables:

* **Global Scope**

    - A variable takes on the global scope when it is defined at the first top-most block {} in your source file

    - For most cases, the first top-most block {} in any Java source file will be your class block

* **Local Scope**

    - A variable takes on a local scope when it is defined in any block {} inside the first top-most block {} in your source file

    - This can be your main method block, any if, elseif, and else statement blocks, loop blocks, function blocks, etc.

### Using variable scope in your source file

{} (curly braces) where a variable in initialized or declared define the scope of said variable.

```java
{ // Scope 1
    int a = 1; // Global scope: variable a will be available in: Scope 1, 2, 3, and 4

    { // Scope 2
        int b = 2; // Local scope: variable will be available in: Scope 2, 3, and 4

        { // Scope 3
            int c = 3; // Local scope: variable will be available in: Scope 3 and 4

            { // Scope 4
                int d = 4; //Local scope: variable will be available in: Scope 4
            }

        }
    }
}
```

### Best practices to follow for variable scope

- Create variables in the appropriate local scope where they will be utilized

- Limit the creation of variables at the global scope level

## Loops

### Loop

If you want to perform the same task multiple times in your program, a loop is the right tool for the job. A loop is used to run  one or more lines of source code in a repeated fashion. Every loop can be broken down into two main parts:

* **Execution condition**

A boolean expression which controls when the body of the loop will be executed.

    - When true -> run body of loop

    - When false -> stop running body of loop

* **Body of loop**

Contains the lines of source code to be executed repeatedly until the execution condition is not true anymore

### Using loops in your source file

Java provides 4 different types of loops:

- **while loop**

Use a while loop when you want to run the loop body until a boolean condition is met or unmet

- **for loop**

Use a for loop when you want to run the loop body a certain number of times

- **do while loop**

Use a while loop when you want the loop to run at least once regardless of execution condition

- **forEach loop** --> will revisit after covering arrays

### Best practices to follow for loops

- Verify your logic before running your program as you can end up with an infinite loop (a loop that never stops running)

- Use the appropriate loop for the task as it makes your source logic easy to follow and understand for you and your peers in the future

### Keywords special to loops

There are two keywords which can be used inside of loops to control its behavior:

* **break**

    Using the break keyword inside a loop block will exit you of said loop block (not the program) immediately

* **continue**

    Using the continue keyword inside a loop block will take back to the execution condition of loop and will skip any instructions below within the loop
