public class Loops1 {
    
    public static void main(String[] args) {
        /*
         int startingNum = 0;
         // an infinite loop is a loop that never stops executing
         // its execution condition will never be false

            while(startingNum <= 10) { // execution condition: true - loop contiunes to run false loop stops running
                System.out.println(startingNum);
            }
        */

        /* counter: allows us to control the number of times a loop runs
        int startingNum = -5;
         // while loop: to print out numbers from -5 to 50
        while(startingNum <= 50) {
            System.out.println(startingNum);
            startingNum += 1;
        }
        */

        /*
        // for loop: is a more advanced while
        // keeps track of and provides a counter variable in its condition
        // along with the execution condition and with means to update the
        // value of the counter
        for(int i = 0; i <= 10; i++) {
            System.out.println(i);
        }
        */
        /*
        // do while: will always runs the loop at least once
        // even if the execution condition is not true
        int startingNum = 15;

        do {
            System.out.println("I still run :p");
            startingNum -= 1;
        } while(startingNum > 10);

        startingNum = 50;

        while(startingNum > 10) {
            System.out.println("WHy you no run me!");
            startingNum -= 1;
        }
        */

        // for loop that prints out numbers from 10 to 0
        for(int i = 10; i >= 0; i--) {
            System.out.println(i);
        }
    }
}
