/*
    Examples showing usage of while and for loops
    to print out numbers 1 to 100
*/
package loops;

public class Loops {

    public static void main(String[] args) {
        // external counter which keeps track of how many times the loop has run or should be run
        int counter = 1; // defines how many numbers have I printed so far
        int numbersToPrint = 100; // defines how many numbers to print

        System.out.println("Output from while loop");
        System.out.println("========================");
        while(counter <= numbersToPrint) // Execution expression aka boolean expression
        { // Loop block aka body
            System.out.println(counter);
            counter += 1;
        }
        System.out.println("========================");

        System.out.println("Output from for loop");
        System.out.println("========================");

        // The counter is moved inside the for loop's execution statement and the increment of the counter as well
        for(int forLoopcounter = 1; forLoopcounter <= numbersToPrint; forLoopcounter++)
        {
            System.out.println(forLoopcounter);
        }
        System.out.println("========================");
    }
}
