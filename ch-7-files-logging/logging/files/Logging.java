/*
    Examples demonstrating working with files
*/

package logging.files;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.io.IOException;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class Logging {

    public static void main(String[] args) {
        // Defines the location and file name and extension for a file
        Path toDoListFilePath = Paths.get("./toDoList.txt");

        try {
            // Checks if a file already exists given its path
            boolean doesFileExist = Files.exists(toDoListFilePath);

            // If the files does not exist, then we want to create the file
            if(!doesFileExist) {
                // Creates a file given a path to the desired file
                Files.createFile(toDoListFilePath);
            } else {
                System.out.println("File: " + toDoListFilePath + " already exists. Skipping creation...");
            }
            
            String task1 = "Homework\n";
            String task2 = "Graduate\n";
            String task3 = "Haircut\n";
            /*
                Writing and Appending: 
                    Process of taking data from our program/application and storing it in a file
            */
            // Writing to a file: Replaces existing data in file with the new data everytime
            Files.write(toDoListFilePath, task1.getBytes()); // First the data Homework gets added to the file
            Files.write(toDoListFilePath, task2.getBytes()); // Then the data Homework is replaced by the data Graduate in the file

            // Appending to a file: Adds the new data to the file and retains/keeps the old data in the file as well
            Files.write(toDoListFilePath, task3.getBytes(), StandardOpenOption.APPEND);

            ArrayList<String> toDoTasks = new ArrayList<String>() {{
                add("Eat\n");
                add("Sleep\n");
                add("Repeat\n");
            }};
            // Loop over toDoTasks in list and append to a file
            for(int i = 0; i < toDoTasks.size(); i++) {
                String toDoTask = toDoTasks.get(i);
                if(i == 0) {
                    // Old data Graduate and Haircut will be throw away to be replaced by the Eat
                    Files.write(toDoListFilePath, toDoTask.getBytes());
                }
                else {
                    // All new data that is not the first piece of data will be appended to the file
                    Files.write(toDoListFilePath, toDoTask.getBytes(), StandardOpenOption.APPEND);
                }
            }

            // Reading: Process of taking data from a file and bringing it into our program/application
            List<String> toDoList = Files.readAllLines(toDoListFilePath);

            System.out.println("Tasks to be completed:");
            for(String toDoListTask: toDoList) {
                System.out.println(toDoListTask);
            }

        } catch(IOException ioE) {
            System.out.println("Failed to create file: " + toDoListFilePath);
            System.out.println(ioE);
        } catch(Exception e) {
            System.out.println(e);
        }
    }
}
