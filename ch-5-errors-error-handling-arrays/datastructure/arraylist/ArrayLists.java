/*
    Examples demonstrating use of arrays
    with take home exercise
*/

package datastructure.arraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ArrayLists {

    public static void main(String[] args) {
        // declare an arraylist, no values initialized
        ArrayList<Integer> studentIdNumbers1 = new ArrayList<Integer>();

        // create and initialize an array with values of specified data type
        ArrayList<Integer> studentIdNumbers2 = new ArrayList<Integer>(Arrays.asList(10, 2, 13, 45));
        ArrayList<Integer> studentIdNumbers3 = new ArrayList<Integer>() {{
            add(1);
            add(2);
        }};

        // Size of arraylist -> Gets the current number of elements inside the arraylist
        System.out.println("Size of studentIdNumbers1: " + studentIdNumbers1.size());
        System.out.println("Size of studentIdNumbers2: " + studentIdNumbers2.size());
        System.out.println("Size of studentIdNumbers3: " + studentIdNumbers3.size());

        System.out.println("");

        // Accessing data in arraylist using index
        System.out.println(studentIdNumbers2.get(0)); // Getting first value in arraylist
        System.out.println(studentIdNumbers2.get(1)); // Getting second value in arraylist
        System.out.println(studentIdNumbers2.get(2)); // Getting third value in arraylist
        System.out.println(studentIdNumbers2.get(3)); // Getting fourth value in arraylist

        System.out.println("");

        try {
            System.out.println(studentIdNumbers2.get(4)); // Exception: java.lang.IndexOutOfBoundsException is thrown
        } catch(IndexOutOfBoundsException indexOutOfBoundsException) {
            System.out.println("Invalid index: The index you are trying to use does not exist for arraylist studentIdNumbers2");
            System.out.println(indexOutOfBoundsException);
        }
        System.out.println("");
        
        // Set or update the value inside an arraylist using the appropriate index
        studentIdNumbers3.set(0, 20);
        studentIdNumbers3.set(1, 30);

        // Array style for loop to print out all elements in an arraylist: studentIdNumbers3
        System.out.println("Elements in studentIdNumbers3 arraylist");
        for(int i = 0;i < studentIdNumbers3.size();i++) {
            System.out.println(studentIdNumbers3.get(i));
        }

        // Arraylist style for loop to print out all elements in an arraylist: studentIdNumbers2
        System.out.println("Elements in studentIdNumbers2 arraylist");
        /*  For each element inside arraylist studentIdnUmbers2, store each element in variable
            studentId, then run the statements inside the loop, then update studentId to next
            element in arraylist studentIdNumbers2 and keep doing that until there are no more
            elements left inside the arraylist
        */
        for(int studentId : studentIdNumbers2) {
            System.out.println(studentId);
        }

        System.out.println("");

        // Adding elements to an arraylist - The new elements get added to the end of the arraylist
        studentIdNumbers3.add(1);
        studentIdNumbers3.add(15);

        // ArrayList provide a lot useful features than arrays

        /*  Contains method checks if specificed element exists inside the arraylist
            Returns true if the specified element exists within the arraylist
            Returns false if the specified element does not exist within the arraylist
        */
        System.out.println("Does studentIdNumbers2 contain student ID 2? " + studentIdNumbers2.contains(2)); // Returns true
        System.out.println("Does studentIdNumbers2 contain student ID 20? " + studentIdNumbers2.contains(20)); // Returns false

        // Sorts the elements in the arrayList in ascending order in place
        Collections.sort(studentIdNumbers3);

        System.out.println("studentIdNumbers3 sorted in ascending order");
        for(int studentId : studentIdNumbers3) { System.out.println(studentId); }

        // Sorts the elements in the arrayList in descending order in place
        Collections.sort(studentIdNumbers3, Collections.reverseOrder());

        System.out.println("studentIdNumbers3 sorted in descending order");
        for(int studentId : studentIdNumbers3) { System.out.println(studentId); }

        // Reverses the elements in the arrayList in last to first
        Collections.reverse(studentIdNumbers2);

        System.out.println("studentIdNumbers2 sorted in reverse order");
        for(int studentId : studentIdNumbers2) { System.out.println(studentId); }

        /*
            Take home exercise on arraylist

            - Create an arraylist that stores numbers 1 to 10
            - Create another arraylist that contains each value in the above array incremented by 10
            - Print out all values in this new summation array

            For example:
                - Given following input arraylist: 1|2|3|4|5|6|7|8|9|10
                - Create and print out following output arraylist: 11|12|13|14|15|16|17|18|19|20
        */
    }
}
