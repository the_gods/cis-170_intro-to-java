/*
    Examples demonstrating use of arrays
    with take home exercise
*/

package datastructure.arrays;

public class Arrays {

    public static void main(String[] args) {
        // declare an array with a fixed size, no values initialized
        char[] englishAlphabet1 = new char[10];

        // create and initialize an array with values of specified data type
        char[] englishAlphabet2 = {'a', 'b', 'c', 'd', 'e'};
        char[] englishAlphabet3 = new char[]{'a', 'b', 'c', 'd', 'e'};

        System.out.println("Size of array englishAlphabet1: " + englishAlphabet1.length); // Gets size of array: maximum values array can store
        System.out.println("Size of array englishAlphabet2: " + englishAlphabet2.length); // Gets size of array: maximum values array can store
        System.out.println("Size of array englishAlphabet3: " + englishAlphabet3.length); // Gets size of array: maximum values array can store

        System.out.println("");

        // Accessing data in arrays using index
        System.out.println(englishAlphabet2[0]); // Getting first value in array
        System.out.println(englishAlphabet2[1]); // Getting second value in array
        System.out.println(englishAlphabet2[2]); // Getting third value in array
        System.out.println(englishAlphabet2[3]); // Getting fourth value in array
        System.out.println(englishAlphabet2[4]); // Getting fifth value in array

        System.out.println("");

        try {
            System.out.println(englishAlphabet2[5]); // Exception: ArrayIndexOutOfBoundsException is thrown
        } catch(ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsE) {
            System.out.println("Invalid index: The index you are trying to use does not exist for array englishAlphabet2");
            System.out.println(arrayIndexOutOfBoundsE);
        }
        System.out.println("");

        // Set or update the value inside an array using the appropriate index
        englishAlphabet1[0] = 'a';
        englishAlphabet1[1] = 'b';
        englishAlphabet1[2] = 'c';
        englishAlphabet1[3] = 'd';
        englishAlphabet1[4] = 'e';
        englishAlphabet1[5] = 'f';
        englishAlphabet1[6] = 'g';
        englishAlphabet1[7] = 'h';
        englishAlphabet1[8] = 'i';
        englishAlphabet1[9] = 'j';

        System.out.println("Elements in array englishAlphabet1:");
        // Print out all values inside englishAlphabet1 array
        for(int i = 0;i < englishAlphabet1.length;i++) {
            System.out.println(englishAlphabet1[i]);
        }
        /*
            Take home exercise on arrays

            - Create an array that stores numbers 1 to 10
            - Create another array that contains each value in the above array incremented by 10
            - Print out all values in this new summation array

            For example:
                - Given following input array: 1|2|3|4|5|6|7|8|9|10
                - Create and print out following output array: 11|12|13|14|15|16|17|18|19|20
        */

        int[] inputNumbers = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int inputNumbersLength = inputNumbers.length;
        int[] outputNumbers = new int[inputNumbersLength];

        /* Adding 10 to each element in inputNumbers array and
            storing it in outputNumbers array
        */
        for(int i = 0;i < inputNumbersLength;i++) {
            outputNumbers[i] = inputNumbers[i] + 10;
        }

        /*
            i = 0; 0 < 10 => true run loop statements then i++
            i = 1; 1 < 10 => true run loop statements then i++
            i = 2; 2 < 10 => true run loop statements then i++
            i = 3; 3 < 10 => true run loop statements then i++
            i = 4; 4 < 10 => true run loop statements then i++
            i = 5; 5 < 10 => true run loop statements then i++
            i = 6; 6 < 10 => true run loop statements then i++
            i = 7; 7 < 10 => true run loop statements then i++
            i = 8; 8 < 10 => true run loop statements then i++
            i = 9; 9 < 10 => true run loop statements then i++
            i = 10; 10 < 10 => false exit loop

            Print out all elements in inputNumbers and outputNumbers arrays
            from first element to last element
        */
        for(int i = 0;i < inputNumbersLength;i++) {
            System.out.println(inputNumbers[i] + " -> " + outputNumbers[i]);
        }

        /*
            i = 9; 9 > -1 => true run statemetns inside loop then i--
            i = 8; 8 > -1 => true run statemetns inside loop then i--
            i = 7; 7 > -1 => true run statemetns inside loop then i--
            i = 6; 6 > -1 => true run statemetns inside loop then i--
            i = 5; 5 > -1 => true run statemetns inside loop then i--
            i = 4; 4 > -1 => true run statemetns inside loop then i--
            i = 3; 3 > -1 => true run statemetns inside loop then i--
            i = 2; 2 > -1 => true run statemetns inside loop then i--
            i = 1; 1 > -1 => true run statemetns inside loop then i--
            i = 0; 0 > -1 => true run statemetns inside loop then i--
            i = -1; -1 > -1 => false exit loop

            Print out all elements in inputNumbers and outputNumbers arrays
            from last element to first element
        */
        for(int i = inputNumbersLength-1;i > -1;i--) {
            System.out.println(inputNumbers[i] + " -> " + outputNumbers[i]);
        }
    }
}
