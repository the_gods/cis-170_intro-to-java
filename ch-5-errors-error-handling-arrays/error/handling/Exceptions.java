/*
    Example demostrating use of try/catch block and throw keyword for
    handling exceptions in our program
*/
package error.handling;

import java.util.Scanner;
import java.util.InputMismatchException;

public class Exceptions {

    public static void main(String[] args) {
        int age = -1;

        while(true) { // infinite loop, run until user enters a valid age
            try {
                Scanner userInput = new Scanner(System.in);
                System.out.print("Please enter your age: ");
                age = userInput.nextInt();

                if(age < 0) { // If age value is invalid aka negative
                    throw new Exception("Age cannot be a negative number. You entered: " + age);
                }

                System.out.println("Your age is: " + age);
                /*
                    break out of loop once user has entered a valid age and that age has been printed
                    do not forget break, if you do will be stuck in inifinte loop
                */
                break;
            } catch(InputMismatchException inputMismatchException) {
                System.out.println("Please enter a whole number for your age");

                System.out.println(inputMismatchException.getMessage()); // print out only the message of the exception
                System.out.println(inputMismatchException); // print out type of exception and the message of the exception
                inputMismatchException.printStackTrace(); // prints out the stack trace of the program which includes type and message of exception and source and line numbers
            } catch(Exception e) {
                System.out.println("Please enter a whole number for your age");

                System.out.println(e.getMessage()); // print out only the message of the exception
                System.out.println(e); // print out type of exception and the message of the exception
                e.printStackTrace(); // prints out the stack trace of the program which includes type and message of exception and source and line numbers
            }
        }
    }
}
