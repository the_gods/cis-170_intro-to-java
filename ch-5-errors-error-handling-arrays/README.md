# Ch 5 - Errors, Error Handling, and Arrays

* [Errors](#markdown-header-errors)

## Errors

When creating a software program or application, be it by an experienced or novice developer, causing and/or encountering **errors** is an inevitable part of the software engineering lifecycle.

### Error

An error is a **mistake** or **fault** in the program that causes a program or application to **behave unexpectedly**. Examples of this unexpected behavior could be:

* Program crashes completly and does not run or will not start
    - This is the best kind of error as it is usaully the easiest one to resolve

* Program does not produce correct result/output

* Program works as expected sometimes and other times it misbehaves
    - This is the worst kind of error as it is usually to most difficult to resolve

**Note**: 99.9999999% of the time errors are caused due to mistakes made by humans as "to err is human"

There are three main types of errors which can occur in any program/application:

1. **Syntax error**: occurs due to the rules of the language (syntax) not being respected

2. **Semantic error**: occurs due to improper use of program statements

3. **Logical error**: occurs due to the software specification not being respected. 

    - Commonly known as **bug** in software
    - This type of error is the hardest to detect and resolve
    - The program/application is compiled and executed without errors, but does not generate the requested result.

The above mentioned type of errors can occur and be detected during the following:

1. Compile time: is when the program/application is being compiled using the `javac` command

    - **Compile time errors** occur and are detected during the compilation phase
    - **Syntax errors** and **some semantic errors** are found out at complie time

2. Runtime: is when the program/application is being executed (run) using `java` command

    - **Runtime errors** occur and are detected during the execution phase
    - **Some semantic errors** and **logical errors** are found out at runtime

### Examples of errors in your source file

* **Syntax error** at compile time - missing semicolon (;) at the end of a statement

```java
int i = 0 // missing semicolon
```
**Error Message**:

![Compile time syntax error - missing semicolon](./imgs/syntax-error-missing-semicolon.png)

* **Semantic error** at compile time - incrementing a variable without intializing it with a value

```java
int i; // no value set for i
i++; // incrementing variable which has no value
```
**Error Message**:

![Compile time semantic error - incrementing without initializing](./imgs/semantic-error-compile-time-incrementing-without-initializing.png)

* **Logical error** at runtime - subtracting two values instead of addition to calculate sum of two numbers

```java
int a = 5;
int b = 3;
int sumOfAB = a - b; // subtracting instead of adding will yield incorrect result
```

**Error Message**:

![Runtime logical error - subtracting instead of adding](./imgs/logical-error-subtracting-instead-of-adding-to-compute-sum.png)

### Best Practices to follow for resolving errors

* When you encounter errors, remain calm and read and try to understand the error message as Java is trying to tell you the following:

    - Location of error message in your source file

    - Cause of the error

    - Sometimes, it is also able to provide a suggestion on resolving the error(s)

* Always start with the first error and attempt to resolve

    - Most of the time other errors are caused due to the first error

* **Syntax and semantic errors are very common** and you should be able to find plenty of information about these errors on **StackOverflow or the Internet**

* **Logical errors** are introduced due to a developer's misunderstanding of between the expected behavior and actual behavior of a program/application

    - Extremely difficult to detect and resolve this type of error

## Error handling

**Java refers to these errors in Exceptions**. The reason for being called exceptions as they indicate when a program/application does not work or respond in its normal usual expected manner.

So far all the programs we have written do not deal with exceptions that occur. Currently, when an exception occurs the program prints out an ugly error messge and crashes. Can we do better?

### Handling Errors

Since expections are bound to happen in any and all Java programs or applications it is important that we have a way to handle these exceptions. Java provides use with a `try/catch` blocks and `throw` keyword in order to deal with exceptions which may occur.

**How your source runs inside a try catch block?**

1. Everything inside the try block will be executed by Java

2. While executing statements inside the try block exceptions can occur

3. If an exception occurs, that exception will be thrown

4. The thrown exception will be caught by the catch block so it can be handled

5. The source inside the catch block is run once the exception has occured and been caught

**How your source runs when you throw an exception manually?**

1. Running the `throw` keyword anywhere in your source file immediately causes an error and throws an exception

2. The thrown exception is caught and handled by the catch block closest to the scope where the exception was thrown

**What are some benefits to handling exceptions in our program?**

- Provide more informative and easier to read and understand error messages to our users

- Attempt to fix or address the expection automatically without user interaction or provide the user an opportunity to fix their mistake

- Ensure that our program does not crash but instead exits gracefully

### Using error handling in your source file

**Try catch blocks and throw keyword usage format**:

```java
try {
    // lines of source to run your program
    // go here: inside the try block

    throw ExceptionClassName("error message");

} catch(ExceptionClassName exceptionClassVariableName1) {
    // lines of source to run when an ExceptionClassName occurs
    //go here: inside the catch block
} catch(ExceptionClassName exceptionClassVariableName2) {
    // lines of source to run when an ExceptionClassName occurs
    //go here: inside the catch block
} catch(ExceptionClassName exceptionClassVariableNameN) {
    // lines of source to run when an ExceptionClassName occurs
    //go here: inside the catch block
}
```

**Try catch block and throw keyword usage example**:

```java
import java.util.Scanner;

try {
    Scanner userInput = new Scanner(System.in);
    System.out.print("Please enter your age: ");
    // Exception is automatically thrown by nextInt if something other than an int is entered and caught by catch block
    int age = userInput.nextInt();

    if(age < 0) {
        // Manually throwing an exception when age entered by user is a negative number
        throw new Exception("Age cannot be a negative number");
    }

} catch(Exception e) { // Most generic exception. Accounts of all different kinds of exceptions that can be thrown from the try block
    System.out.println("Please enter a whole positive number for your age");

    // Using and getting information out of the caught exception
    System.out.println(e.getMessage()); // Prints out the only message for the exception which was caught
    System.out.println(e); // Prints out the type and message for the exception which was caught
    e.printStackTrace(); // Prints out the most detailed informastion about the exception which was caught: type, message, source and line numbers
}
```

### Best practices for error handling

* Always use try catch blocks when working with something that is outside of your Java program

    - For example: Getting input from the user, working with files on any machine, downloading or uploading something from the Internet, etc.

* Try and catch blocks can only exist together

    - Each try block requires at least 1 catch block

    - A try block can have more than one catch block to handle different types of exceptions
