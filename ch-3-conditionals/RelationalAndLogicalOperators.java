public class RelationalAndLogicalOperators {
  public static void main(String[] args) {

    int age = 20;
  
    // Equality and non-equality operators
    // can be used for int, float, double, boolean, and char
    System.out.println(age == 20); // true
    System.out.println(age != 20); // false

    age = 12;
    System.out.println(age == 20); // false
    System.out.println(age != 20); // true

    age = 21;
    // greater than and greater than or equal to
    System.out.println(age > 21); // false
    System.out.println(age >= 21); // true

    age = 65;

    // less than and less than or equal to
    System.out.println(age < 70);  // true
    System.out.println(age <= 65); //true
    System.out.println(age < 65); // false

    System.out.println("-------------------------------------");
    // String comparison
    String a = "ABC";
    String b = "ABC";

    System.out.println("ABC".equals("ABC"));
    System.out.println("ABC".equals("DFG"));
    System.out.println(a.equals(b));
  }
}
