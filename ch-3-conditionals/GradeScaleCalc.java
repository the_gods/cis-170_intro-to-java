import java.util.Scanner;

public class GradeScaleCalc {

    public static void main(String[] args) {

        // Getting test score as input from user
        Scanner userInput = new Scanner(System.in);
        System.out.print("Please enter your test score: ");
        double testScore = userInput.nextDouble();

        if(testScore >= 90.0 && testScore <= 100.0) {
            /*
                [90.0 - 94.0) -> A-
                [94.0 - 97.0) -> A
                [97 - 100] -> A+
            */
            if (testScore >= 90.0 && testScore < 94.0) {
                System.out.println("A-"); 
            } else if(testScore >= 94.0 && testScore < 97.0) {
                System.out.println("A");
            } else {
                System.out.println("A+");
            }

        } else if(testScore >= 80.0 && testScore < 90.0) {
            System.out.println("You earned an B with score: " + testScore);
        } else if(testScore >= 70.0 && testScore < 80.0) {
            System.out.println("You earned an C with score: " + testScore);
        } else if(testScore >= 60.0 && testScore < 70.0) {
            System.out.println("You earned an D with score: " + testScore);
        } else if(testScore >= 0 && testScore < 60.0) {
            System.out.println("You earned F with score: " + testScore);
        } else {
            System.out.println("Invalid test score: " + testScore);
            System.out.println("Test scores cannot be negative");
        }
    }
}
