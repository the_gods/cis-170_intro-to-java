import java.util.Scanner;

public class GradeScale {

    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);

        System.out.print("Please enter a letter grade: ");
        char letterGrade = userInput.next().charAt(0);

        /*
            Grading Scale for a class:
                A -> 90 - 100
                B -> 80 - 89
                C -> 70 - 79
                D -> 60 - 69
                F -> 0 - 59
        */
        switch(letterGrade) {
            case 'A':
            case 'a':
                System.out.println("90 - 100");
                break;
            case 'B':
            case 'b':
                System.out.println("80 - 89");
                break;
            case 'C':
                System.out.println("70 - 79");
                break;
            case 'D':
                System.out.println("60 - 69");
                break;
            case 'F':
                System.out.println("0 - 59");
                break;
            // runs if value does not match any of the cases above
            default:
                System.out.println("Invalid Letter Grade: " + letterGrade);
                System.out.println("Please enter in uppercase or lowercase letter grades: A/a, B/b, C/c, D/d, or F/f");
                break;
        }
    }
}
