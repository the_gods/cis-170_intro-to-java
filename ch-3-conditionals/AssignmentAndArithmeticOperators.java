public class AssignmentAndArithmeticOperators  {

  public static void main(String[] args) {
    
    // assignment operator (equals)
    // associate a value with a name and type
    int age = 20;
    System.out.println(age);
    // use the assignment operator and name of var
    // to update its value
    age = 21;
    System.out.println(age);

    // Binary Arthimetic Operators: +, -, *, /, %
    System.out.println("(1+1): " + (1+1));
    System.out.println("(1-1): " + (1-1));
    System.out.println("(1*1): " + (1*1));
    System.out.println("(1/1): " + (1/1));
    System.out.println("(1%1): " + (1%1));

    // Unary Arthimetic Operators: +, -, ++, --
    // by default all numbers are positive
    System.out.println(+3);
    System.out.println(-3);
    // Increment operator: add 1 to value
    System.out.println(age++); // 21
    System.out.println(++age); // 23
    // Decrement operator: subtract 1 from value
    System.out.println(age--); // 23
    System.out.println(--age); // 21
    age--; // 20 -> age = age - 1;
    --age; // 19
    System.out.println("Age is updated: " + age);

  // Shorthand for updating the value of a var
  // with a particular operation and number
    int height = 5;
    System.out.println("Height: " + height);
    height = height + 1; // 6
    System.out.println("Height: " + height);
    height += 5; // height = height + 5 -> 11
    System.out.println("Height: " + height);
    height -= 4; // height = height - 4 -> 7
    System.out.println("Height: " + height);
    height *= 2; // height = height * 2 -> 14
    System.out.println("Height: " + height);
    height /= 10; // height = height / 10 -> 1
    System.out.println("Height: " + height);
    height %= 2; // height = height % 2 -> 1
    System.out.println("Height: " + height);
    height /= 2; // height = height / 2 -> 0
    System.out.println("Height: " + height);
  }
}
