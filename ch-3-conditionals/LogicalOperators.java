public class LogicalOperators {

    public static void main(String[] args) {
        
        // && (and), || (or), ! (not)
        boolean isPersonOfLegalDrinkingAge = true;

        System.out.println(isPersonOfLegalDrinkingAge);
        System.out.println(!isPersonOfLegalDrinkingAge);

        boolean isRegistered = false;
        System.out.println(isRegistered); // false
        System.out.println(!isRegistered); // true

        int age = 20;

        System.out.println(age == 21); // false
        System.out.println(!(age == 21)); // true

        // && conditions on both sides must be true for the whole thing to be true
        age = 21;
        String country = "USA";

        System.out.println(age >= 21 && country == "USA"); // true

        age = 20;
        System.out.println(age >= 21 && country == "USA"); // false

        age = 21;
        country = "Canada";
        System.out.println(age >= 21 && country == "USA"); // false

        age = 19;
        System.out.println(age >= 21 && country == "USA"); // false

        // || only condition needs to be true for the whole expression to be true
        age = 21;
        String country = "USA";

        System.out.println(age >= 21 || country == "USA"); // true

        age = 20;
        System.out.println(age >= 21 || country == "USA"); // true

        age = 21;
        country = "Canada";
        System.out.println(age >= 21 || country == "USA"); // true

        age = 19;
        System.out.println(age >= 21 || country == "USA"); // false

    }
}