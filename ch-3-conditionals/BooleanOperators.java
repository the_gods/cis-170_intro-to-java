/*
	Examples of boolean operators
		- Equality
		- Logical
*/

package bool.operators;

public class BooleanOperators {
	public static void main(String[] args) {
		// Equals operator
		System.out.println("1 == 1: " + (1 == 1));
		System.out.println("1 != 1: " + (1 != 1));
		// Not equals operator
		System.out.println("!(1 == 1): " + !(1 == 1));
		System.out.println("!(1 != 1): " + !(1 != 1));
		
		// Less than, Less than equal to
		System.out.println("1 < 2: " + (1 < 2));
		System.out.println("1 <= 1: " + (1 <= 2));
		// Greater than, Greater than equal to
		System.out.println("110 > 22: " + (110 > 22));
		System.out.println("22 >= 22: " + (22 >= 22));
		
		// String equality
		String str1 = "HE";
		String str2 = "HE";
		System.out.println("str1.equals(str2): " + (str1.equals(str2)));

		str2 = "SHE";
		System.out.println("str1.equals(str2) (SHE): " + (str1.equals(str2)));
		
		// && (and) logical operator
		System.out.println("(1 == 1) && (1 != 2): " + ((1 == 1) && (1 != 2));
		System.out.println("(1 == 1) && (1 != 1): " + ((1 == 1) && (1 != 1));
		
		// || (or) logical operator
		System.out.println("(1 < 1) || (1 <= 1): " + ((1 < 1) && (1 <= 1));
		System.out.println("(1 < 1) || (1 == 20): " + ((1 < 1) && (1 == 20));
	}
}
