/*
	Examples of if else, if elseif else statements
	Examples of switch case statements
*/

package bool.conditionals;

import java.util.Scanner;

public class Conditionals {
	public static void main(String[] args) {
		//Boolean Expression == Condition
		/*
			if(condition) {
				if condition is true then the if code block will run;
			}
			else {
				if no condition is true then the else code block will run;
			}
		*/
		System.out.print("Please enter your age: ");
		Scanner userInput = new Scanner(System.in);
		int age = userInput.nextInt();
		
		if(age > 0 && age < 18) {
			System.out.println("You are a minor");
		}
		else {
			System.out.println("You are an adult");
		}
		System.out.println("");
		/* if, elseif, and else statement
			only one of the code blocks will execute
			
			if(condition1) {
				only run this if condition1 is true;
			}
			elseif(condition2) {
				only run this if condition2 is true;
			}
			elseif...n {
				
			}
			else {
				only run if none of the above conditions are true;
			}
			
			- Cannot have else or elseif without and if
			- Can have any number of elseif below and if condition
		*/
		
		if(age > 0 && age < 18) {
			System.out.println("You are a minor");
		}
		else if(age >= 18) {
			System.out.println("You are an adult");
		}
		else {
			// Good error message: Provide invalid input error along with user inputted value
			// Provide the user with a message telling them the correct/valid value being expected
			System.out.println("Invalid age: " + age);
			System.out.println("Please enter a positive whole number for your age");
		}
		
		/*
			Switch case statement
			- Use this when your condition is very simple and singular
			- Case is the value you want to use for comparison in your condition
			- The types of the case can be: int, char, string
			- default must be the last case in your switch statement

			switch(case) {
				case Var/Val:
					only run this line if case matches Var/Val1;
				case Var1/Val1:
					only run this line if case matches Var1/Val1;
				case .... n:
					only run this line if case matches n;
				default:
					only run this if non of the cases above match;
			}
		*/
		
		System.out.print("Please enter the grade you want to earn: ");
		char letterGrade = userInput.next().charAt(0);
		// Change case of input character to uppercase
		letterGrade = Character.toUpperCase(letterGrade);

		switch(letterGrade) {
			case 'A':
				System.out.println("You must earn 90-100 for an " + letterGrade);
				break;
			case 'B':
				System.out.println("You must earn 80-89 for an " + letterGrade);
				break;
			case 'C':
				System.out.println("You must earn 70-79 for an " + letterGrade);
				break;
			case 'D':
				System.out.println("You must earn 60-69 for an " + letterGrade);
				break;
			case 'F':
				System.out.println("You must earn 59-0 for an " + letterGrade);
				break;
			default:
				System.out.println("You enetered an invalid letter grade: " + letterGrade);
				System.out.println("Please enter a valid grade: A, B, C, D, or F");
		}
	}
}
