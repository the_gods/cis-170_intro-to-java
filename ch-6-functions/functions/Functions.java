/*
    Examples demostrating the creating and use of functions
*/
package functions;

import functions.Calculations;

public class Functions {

    public static void main(String[] args) {
        /* Calling a static pure function in a different file in the same package
            Format:
                NameOfTheOwningClass.nameOfTheFunction(arg1, arg2, ... argN);
        */
        double[] userInputs = Calculations.getNumberInputsFromUser();

        /* 
            Calling a static function in a different file in the same package. 
            Format:
                NameOfTheOwningClass.nameOfTheFunction(arg1, arg2, ... argN);
        */
        Calculations.sumTwoNumsPrintResult(userInputs[0], userInputs[1]);
    }
}
