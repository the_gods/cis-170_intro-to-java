/*
    Contains helper functions to perform basic mathematical operations such as:
    addition, subtraction, multiplication, division, etc.
*/
package functions;

import java.util.Scanner;

public class Calculations {

    // As this file only contains helper function, no main function is required this file

    /*
        Format of a function in Java

        [access modifier] [ownership of the function] [return data type] [name of the function](argumentDataType argName1, argDataType, argName2, ... argDataType argNameN) {
            where all the magic happens/ all the work is done inside the
            finally you return the result of running this function
        }

        [access modifier] - In Java there are three types of access modifiers
        1) private - When a funciton is marked private it means it can only be accessed and used within the file where the function was created

        2) protected - When a function is marked protected it means it can be accessed and used package by all files within the same package

        3) public - When a function is marked public it means it can be accessed and used by any and all files within any packages

        [ownership of the function] - There two types of ownership in Java. A function can be owned by a class or object.
        1) static - the function is owned by the class (blueprint of the house)

        2) non static (leave out the static keyword) - the funciton is owned by the object (actual house created from the blueprint)

        [return value data type] - A function can return data of any type that is built-in to Java or the customized types created by developers
        1) int
        2) double
        3) float
        4) char
        5) String
        6) boolean
        7) void - Do not return anything - no result is returned to the caller of the function.

        [name of the function] - Use lowerCamelCase to name all functions we create. Same naming convention used for variables to be used to name functions.

        - In any file, all function names must be unique
        - The name of the funciton should summarize the actions that it performs and the result that it returns

        [arguments (inputs) of the function] - Arguments allow you to pass data such as user input inside functions

        - All arguments are located inside the parenthesis following the name of the funciton
        - A function can have 0 or more arguments that it can accept
        - If your function does not accept any arguments but you pass in arguments anyway it will cause an exception
        - Each argument takes on the following format for a function:
        
        <---first argument <--second argument
        (int age,           char sex)
        This function requires two arguments to be passed in when being called

        The below function is a pure function. A pure function is one where given the same set of inputs it returns the same output

        private static boolen isPersonOfLegalAgeToDrink(int age, String countryName) {
            if(age >= 21 && countryName.equals("United States")) {
                return true;
            } else if(age >= 18 && !countryName.equals("United States")) {
                return true;
            } else {
                return false;
            }
        }

        The below funtion is a side-effect (impurse function). A side-effect function is one that does not return a result but instead performs an action outside of your program source. Example: Printing out to the screen, emailing information to someone, storing data in a file or database, etc.

        private static void isPersonOfLegalAgeToDrink(int age, String countryName) {
            if(age >= 21 && countryName.equals("United States")) {
                System.out.println("Yes you are of legal drinking age in " + countryName);
            } else if(age >= 18 && !countryName.equals("United States")) {
                System.out.println("Yes you are of legal drinking age in " + countryName);
            } else {
                System.out.println("Sorry you are not of legal drinking age in " + countryName);
            }
    */

    /*
        Takes 2 args of type double. Calculates the sum of these 2 args.
        Prints out the result which is the sum of the 2 numbers.
    */
    protected static void sumTwoNumsPrintResult(double num1, double num2) {
        double sumOfTwoNums = num1 + num2;
        System.out.println("Sum of " + num1 + " + " + num2 + " = " + sumOfTwoNums);
    }

    /*
        Args: No arguments
        Prompts the user to enter 2 numerical values as doubles,
        puts these user inputted values in an array, and finally
        returns the array that contains the user input
    */
    protected static double[] getNumberInputsFromUser() {
        Scanner userInput = new Scanner(System.in);

        System.out.print("Input 1 (Number): ");
        double num1 = userInput.nextDouble();

        System.out.print("Input 2 (Number): ");
        double num2 = userInput.nextDouble();

        return new double[]{num1, num2};
    }
}
