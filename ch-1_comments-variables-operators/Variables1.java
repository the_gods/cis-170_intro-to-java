public class Variables {

    public static void main(String[] args) {

        /*
            Variable declaration
            - type name;
            Variable assignment
            - name = value;
        */
        int age;
        age = 11;
        System.out.println("Age: " + age);
        // Update value of age
        age = 20;
        System.out.println("Age: " + age);

        /*
            Variable initialization
            - type name = value;
        */
        // int: Whole numbers only
        int numOfClasses = 4;
        System.out.println("Number of classes registered: " + numOfClasses);
        // float: decimal numbers have to end with an F
        // can only track upto 7 decimal places (will round up any extra places)
        float gpa = 3.57111111111111F;
        System.out.println("GPA: " + gpa);
        // double: decimal numbers
        // provides more precision than a float (more accurate)
        double massOfAtom = 2.1232323124244124;
        System.out.println("Mass of atom: " + massOfAtom);

        // boolean: can only have 2 values: true or false
        // used for computer decision making process
        boolean isStudentAcademicStandingGood = true;
        System.out.println("Good Academic Standing: " + isStudentAcademicStandingGood);
        isStudentAcademicStandingGood = false;
        System.out.println("Good Academic Standing: " + isStudentAcademicStandingGood);
        System.out.println("Good Academic Standing: " + isStudentAcademicStandingGood);

        // char: used to store only one letter or special character (~, !, @, etc.)
        char letterGrade = 'A';
        System.out.println("Letter Grade: " + letterGrade);
        // string: used to store 0 or more letter(s) or special character(s) (~, !, @, etc.)
        String fullName = "Bruce Wayne";
        System.out.println("Full Name: " + fullName);
    }
}
