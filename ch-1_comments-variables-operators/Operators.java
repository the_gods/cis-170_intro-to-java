/*
	Examples showing the use of mathematical operators:
	- Unary
	- Binary
*/
public class Operators {

	public static void main(String[] args) {
		// Both of these are positive numbers
		int positiveVal1 = 10;
		int positiveVal2 = +10;
		System.out.println("positiveVal1: " + positiveVal1);
		System.out.println("positiveVal2: " + positiveVal2);

		// Negative numbers
		int negativeVal = -10;
		System.out.println("negativeVal: " + negativeVal);

		// Increment operator. adds 1 to the value
		int newPositiveVal1 = ++positiveVal1;
		System.out.println("newPositiveVal1: " + newPositiveVal1);

		// Decrement operator. subtracts 1 from the value
		int newNegativeVal1 = --negativeVal;
		System.out.println("newNegativeVal1: " + newNegativeVal1);

		// Short-hand increment operation
		// newPositiveVal1 = newPositiveVal1 + 1;
		newPositiveVal1 += 1;
		System.out.println("short increment val (newPositiveVal1): " + newPositiveVal1);		

		// Short-hand increment operation
		// newNegativeVal1 = newNegativeVal1 - 1;
		newNegativeVal1 -= 1;
		System.out.println("short decrement val (newNegativeVal1): " + newNegativeVal1);

		boolean isDead = true;
		boolean isAlive = false;
		
		System.out.println("isDead: " + isDead);
		System.out.println("isAlive: " + isAlive);

		// Logical negation - only applies to boolean values

		isDead = !isDead;
		isAlive = !isAlive;
		System.out.println("negated isDead: " + isDead);
		System.out.println("negated isAlive: " + isAlive);

		// Addition of 2 numbers
		int sumOfTwoNumbers = 11 + 59;
		System.out.println("Sum of 2 numbers: " + sumOfTwoNumbers);
		System.out.println("Sum of 2 numbers: " + (11+59));

		// Subtraction of 2 numbers
		int minusOfTwoNumbers = sumOfTwoNumbers - 10;
		System.out.println("Subtract of 2 numbers: " + minusOfTwoNumbers);
		System.out.println("Subtract of 2 numbers: " + (sumOfTwoNumbers - 10));

		// Multiplication of 2 numbers
		int multiplyTwoNumbers = minusOfTwoNumbers * 2;
		System.out.println("Multiplication of 2 numbers: " + multiplyTwoNumbers);
		System.out.println("Multiplication of 2 numbers: " + (minusOfTwoNumbers * 2));

		// Division of 2 numbers - returns quotient of result
		int divisionOfTwoNumbers = multiplyTwoNumbers / 60;
		System.out.println("Quotient: " + divisionOfTwoNumbers);
		System.out.println("Quotient: " + (multiplyTwoNumbers / 60));

		// Modulus of 2 numbers - returns remainder of result
		int modulusofTwoNumbers = multiplyTwoNumbers % 60;
		System.out.println("Remainder: " + modulusofTwoNumbers);
		System.out.println("Remainder: " + (multiplyTwoNumbers % 60));
	}
}
