/*
	Examples demoing the creation and use of different
	variables and data types and printing their values
	to the console.
*/
public class Variables {
	public static void main(String[] args) {

		// Whole positive and negative numbers and zero
		int age = 75;
		// Print out value of variable with text
		System.out.println("Age: " + age);

		// Decimal positive and negative numbers and zero
		float temperatureInF = 75.50f;
		System.out.println("Temperature in F: " + temperatureInF);

		// Decimal positive and negative numbers and zero
		double hourlyRate = 19.50;
		System.out.println("Hourly rate: $" + hourlyRate);

		// Supports only two values: true or false
		boolean isRetired = true;
		boolean isDead = false;
		System.out.println("Is Retired? " + isRetired);
		System.out.println("Is Dead? " + isDead);

		// Supports only one alpha or numeric value
		char sex = 'M';
		System.out.println("Sex: " + sex);

		// Supports 2 or more alpha or numeric value
		String name = "Cheetoh Head";
		System.out.println("Name: " + name);
	}
}
