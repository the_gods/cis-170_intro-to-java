# Ch 1 - Comments, Variables, and Operators

* [Comments](#markdown-header-comments)
* [Variables](#markdown-header-variables)
* [Operators](#markdown-header-operators)

## Comments

### Comment

A comment is programmer-readable exaplaination of the computer program.

* Used to **describe and document the source code** which defines your computer program

* Allows humans to better understand the purpose and functionality of the source code and/or program

* Ignored by the compiler and/or interpreter

### Using comments in your source file

Java supports 2 types of comments:

1. **Single-line comments**
    * Limited to one line in your source file
    * Represented with 2 forward slashed (//)

2. **Multi-line comments**
    * Can span over 1 or more lines in your source file
    * Represented with a forward slash, 2 asterisks, and a backward slash (/* */)

```java
// This is a single-line comment in your java file

/*
    This is a multi-line comment
    spanning over multiple lines
    in your java file.
*/
```

### Best Practices to follow for comments

* At the top of every source file, **include a summary describing the purpose and functionality of your program**

    - Use a multi-line comment

* Add a comment above a line of code if it might be difficult to understand its purpose and/or functionlity now or in the future

    - Use a single line comment

Having good descriptive comments is very important, as they will help others on your team and future you to easily read and understand the source code of your program.

## Variables

* **Declaration**: States the type and name of a variable

    - A variable can be declared only once

    - Before you can use a variable, that variable must be declared

```java
// Variable Declaration
int x;
```

* **Assignment**: Setting the value of an already declared variable

    - Throws away the old value of a variable and replaces it with the new value

    - Can only be performed after a variable has been declared

```java
int x;
int y;
// Variable Assignment
x = 10;
y = 20;
// Variable Assignment - update values of variables x and y
x = 20;
y = 10;
```

* **Initialization**: Performs a variable declaration and assignment all in one line

    - Best pratice is to use initializtion when creating a variable

    - If you try to access a variable that has not been declared you will get the following error message:

    ![Variable not declared error message](./imgs/variable-not-declared-error-message.png)

    - If you try to access a variable that has not been assigned a value you will get the following error message:

    ![Variable not assigned a value error message](./imgs/variable-not-assigned-a-value-error-message.png)

    - If you try to re-initialize or re-declare the same variable in your program you will get the following error message:

    ![Variable re-initialization or re-declaration](./imgs/variable-reinitialization-or-redeclaration.png)

```java
// Variable Initialization which has performed a Variable Declaration and Assignment
int x = 10;

// Variable x cannot be declared again. This will cause an error saying: x is already defined in your program
int x = 20;

// Use Variable assignment to update the value of an already delcared variable
x = 20;
```

## Operators

### Operator

An operator is symbol or keyword which performs an action on one more elements to produce a result. Operators perform operations on provided input and return the result as output.

There are two common types of operations performed by operators:

* **Unary operations**

    - Involve only one value
    - Format: \[Operator\]\[Value\] => \[Resulting Value\]

Java supports the following unary operations:

| Operator Name               | Operator Symbol |  Format                                                       |  Result                                                                                                              |
|-----------------------------|-----------------|---------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------|
| Plus                        | +               | +\[ Value1 \]               =>  +\[ Result \]                 |  Indicates positive value (number are positive without this by default). Applies to numerical values only.           |
| Minus                       | -               | -\[ Value1 \]               =>  -\[ Result \]                 |  Indicates negative value (negates an expression). Applies to numerical values only.                                 |
| Increment                   | ++              | \[ NumValVariableName \]++  =>  \[ NumValVariableName+1 \]    | Increments a value by 1. Applies to variables holding numbers only.                                                  |
| Decrement                   | --              | \[ NumValVariableName \]--  =>  \[ NumValVariableName-1 \]    |  Decrements a value by 1. Applies to variables holding numbers only.                                                 |
| Logical complement/Negation | !               | !\[ BooleanValOrVar \]      =>  \[ OppositeBooleanValOrVar \] |  Inverts the value of a boolean. Applies to boolean values (true and false) and to variables holding boolean values. |

* **Binary operations**

    - Involve two values
    - Format: \[Value1\] \[Operator\] \[Value2\] => \[Resulting Value\]

Just like a calculator, Java supports the following binary mathematical operations:

| Operator Name  | Operator Symbol |  Format                                         |  Result                                            |
|----------------|-----------------|-------------------------------------------------|----------------------------------------------------|
| Addition       | +               | \[ Value1 \]  +  \[ Value2 \]  =>  \[ Result \] | Returns the addition of two numbers                |
| Subtraction    | -               | \[ Value1 \]  -  \[ Value2 \]  =>  \[ Result \] | Returns the subtraction of two numbers             |
| Multiplication | *               | \[ Value1 \]  *  \[ Value2 \]  =>  \[ Result \] | Returns the multiplication of two numbers          |
| Division       | /               | \[ Value1 \]  /  \[ Value2 \]  =>  \[ Result \] | Returns the quotient from division of two numbers  |
| Modulus        | %               | \[ Value1 \]  %  \[ Value2 \]  =>  \[ Result \] | Returns the remainder from division of two numbers |

### Using operators in your source file

### Best Practices to follow for operators

* Use parentheses to enfore order of operation when creating mathematical expressions

## Additional Resources

[Assignment, Arithmetic, and Unary Operators](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/op1.html)
