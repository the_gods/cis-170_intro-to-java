import java.util.ArrayList;

public class Lists {

    public static void main(String[] args) {

        // Create an empty list that stores Integers only
        ArrayList<Integer> testScores = new ArrayList<Integer>();

        // Prints out the size of the ArrayList
        System.out.println("Size of list: " + testScores.size());

        // Adding elements to a list
        testScores.add(56);
        testScores.add(98);
        testScores.add(76);

        System.out.println("Size of list: " + testScores.size());

        // Getting elements from a list using their index (location)
        System.out.println("Score 1: " + testScores.get(0));
        System.out.println("Score 2: " + testScores.get(1));
        System.out.println("Score 3: " + testScores.get(2));

        testScores.add(11);
        System.out.println("Size of list: " + testScores.size());

        // Looping over the entire list and printing out all its elements
        for(Integer testScore : testScores) {
            System.out.println(testScore);
        }
        System.out.println("---------------------------------");
        for(int i = 0; i < testScores.size(); i++) {
            System.out.println(testScores.get(i));
        }
    }
}
