package app.userinput;

import java.util.Scanner;

public class UserInput {

    public static void main(String[] args) {

        // Setup scanner to get input from user's keyboard
        Scanner userInput = new Scanner(System.in);

        System.out.print("Please enter your age: ");
        int age = userInput.nextInt();

        System.out.print("Please enter you GPA: ");
        float gpa = userInput.nextFloat();

        System.out.print("Please enter the total value of your financial portfolio: ");
        double totalFinancialPortfolioValue = userInput.nextDouble();

        System.out.print("Are you registered to vote? ");
        boolean isRegisteredToVote = userInput.nextBoolean();

        System.out.print("Please enter your sex: ");
        char sex = userInput.next().charAt(0);

        System.out.print("Please enter email address: ");
        String emailAddress = userInput.next();

        Scanner userInput1 = new Scanner(System.in);

        System.out.print("Please enter full name: ");
        String fullName = userInput1.nextLine();

        System.out.println("Age: " + age);
        System.out.println("GPA: " + gpa);
        System.out.println("Total Value of Financial Portfolio: $" + totalFinancialPortfolioValue);
        System.out.println("Registered to Vote: " + isRegisteredToVote);
        System.out.println("Sex: " + sex);
        System.out.println("Email: " + emailAddress);
        System.out.println("Full Name: " + fullName);
    }
}
