# Ch 2 - Packages, Import, and User Input using Scanner

* [Packages](#markdown-header-packages)
* [Imports](#markdown-header-imports)
* [User Inputs](#markdown-header-user-inputs)

## Packages

Packages allow for the organization of related files, which make up your programs/applications, into a namespace. This is required as software written using the Java programming languages can be composed of hundreds or thousands of individual files and packages make it easier to manage these various files which come together to form your piece of software.

### Package

A package is nothing but a group of nested directories which will contain your source files (.java) and compiled files (.class). A package is represented and seperated by periods in Java. For example, `java.util` means that your files will be placed in the `java/util` directory on your machine's filesystem.

Java provides the `package` keyword which allows us to organize our source and compiled files into appropriate directories aka packages.

### Using packages in your source file

In theory, the name of a package can be infinitely long but it is good practice to limit your package name to 2-3 words aka directories. The format of adding your source files to a package is as follows:

    package directoryName1.directoryName2.....directoryNameN;

Package statement usage example:

```java
package hello.world;
```

### Best Practices to follow for packages

- The package keyword should be located at the very top of your source file

- Use descriptive and meaningful words as your package name

  * The package name should describe who owns the package and/or what is the purpose of this package

  * Example: **adobe.graphics.drawing**

- Do not use package names already being used by the Java programming language and its environment

- Do not use package names already being used by other people or companies

  * This is important especially if you plan to release your software publicly

## Imports

In the field of Software Engineering, one always comes across this common conundrum, when working on a piece of software or application:

* Should I find and re-use source already implemented by someone that meets my needs?

  For majority of software/application development use cases, using some pre-existing piece of software is the best practice. This should be done in the following situations:

  - When doing tasks which are common and well-known across different software applications

  - When doing tasks which are natively supported as part of the programming language environment

  - When you are able to find reliable, well known and supported third-party software that meets your project's needs

Note: Anytime you want to accomplish something in your software project, the first thought in your mind should be: Has anyone come across or done this before? Most of the time you will find that someone has already figured it out and you do not have to reinvent the wheel.

* Should I write or implement source that meets my needs from scratch? or

  This should only be done in the following situations:

  - When doing something highly specialized and/or uncommon

  - When performance-constraints become an issue

  - When you are unable to find any reliable and well-supported third-party software that meets your software's needs

### Importing

Importing is the process of loading and making available existing classes (pre-compiled source), for use within your Java source file. Java provides an **import statement** in order to perform the task of importing.

### Using import statements in your source file

An **import statement** is used to make pre-complied classes in various packages available within your source file. The following is the syntax (order/format) of an **import statement**:

![Import Statement Syntax](./imgs/import-statement-syntax.png)

**Import statement** usage example:

![Import Statement Usage Example](./imgs/example-import-statement-usage.png)

### Best Practices to follow for imports

* Must be located at the top of your source file (above your class statement)

* You can have 0 or more import statements in your source file

* Only import the classes which are required (being used) in your source file

* Using an import **\***  is bad practice

    - Imports all classes in a package even if you might not be using them in your source file

    - Increases the time it takes to compile and run your Java program

        * Has to wait to load all the imported classes into your computer's memory

## User inputs

So far, we have learnt how to display information to the user in our source files.

* **Stdout (Standard Output)**

    - This is the user's console window
    - `System.out.println` allows us to output information for the user to **Stdout**

The programs we have written have been quite simple and boring. This is because our programs lack user interactivity making them quite rigid.

* **Stdin (Standard Input)**

    - This is input coming from the user's keyboard
    - `System.in` allows us to collect input from **Stdin**

### User input

In order to make our programs more interesting, interactive, and useful we need to add prompts for user input within our program's source. The following is best user input practice:

  1. Prompt the user for input

    * Have users enter information by asking questions

  2. Process the input entered by the user

    * Store and handle the answers provided by the user

  3. Utilize the input in a meaningful way

    * Do something with the user's answers to meet the needs or functions of our program

Java provides the **Scanner class** in order to get and process input from the user's keyboard. Here is more information about the **Scanner class**:

- Package: java.util

- Class: Scanner

- Description: A simple text scanner which can parse primitive types and strings using regular expressions

- Javadoc: [Scanner](https://docs.oracle.com/javase/8/docs/api/java/util/Scanner.html)

### Using the Scanner class to get user input in your source file

```java
import java.util.Scanner;

Scanner userInput = new Scanner(System.in);

System.out.print("Get int: ");
int intVal = userInput.nextInt();

System.out.print("Get float: ");
float floatVal = userInput.nextFloat();

System.out.print("Get double: ");
double doubleVal = userInput.nextDouble();

System.out.print("Get boolean: ");
boolean booleanVal = userInput.nextBoolean();

System.out.print("Get char: ");
char charVal = userInput.next().charAt(0);

System.out.print("Get string (one-word): ");
String stringOneWordVal = userInput.next();

System.out.print("Get string (multiple-words): ");
String stringMultipleWordsVal = userInput.nextLine();
```

### Best Practices

* Only create one instance for a set of related user inputs to be collected

* Use the appropriate function to get and process the inputted value as desired data type
