package cis170.tarun.packagedemo;

// Import Scanner class to handle user input
import java.util.Scanner;

public class UserInput {

	public static void main(String[] args) {
		// Create variable that allows for getting input from user's keyboard
		Scanner userInput = new Scanner(System.in);

		// Print message for user asking for desired input
		System.out.print("Please enter your age: ");
		// Get input from user which will be of type int
		int age = userInput.nextInt();

		// Print message for user asking for desired input
		System.out.print("Please enter your annual salary: ");
		// Get input from user which will be of type float
		float annualSalary = userInput.nextFloat();

		// Print message for user asking for desired input
		System.out.print("Please enter your yearly tax amount: ");
		// Get input from user which will be of type double
		double yearlyTax = userInput.nextDouble();

		// Print message for user asking for desired input
		System.out.print("Are you rich? ");
		// Get input from user which will be of type boolean
		boolean isRich = userInput.nextBoolean();

		// Print message for user asking for desired input
		System.out.print("Enter your sex: ");
		// Get input from user which will be of type char
		char sex = userInput.next().charAt(0);

		// Print message for user asking for desired input
		System.out.print("Please enter your nickname: ");
		// Get input from user which will be of type string (only one word)
		String nickname = userInput.next();

		// Create new Scanner variable after every next or nextLine
		Scanner getFullNameInput = new Scanner(System.in);

		// Print message for user asking for desired input
		System.out.print("Please enter your full legal name: ");
		// Get input from user which will be of type string (multiple space seperated words)
		String fullname = getFullNameInput.nextLine();

		System.out.println("Age: " + age);
		System.out.println("Annual Salary: " + annualSalary);
		System.out.println("Yearly Tax: " + yearlyTax);
		System.out.println("Sex: " + sex);
		System.out.println("Nickname: " + nickname);
		System.out.println("Full legal name: " + fullname);
	}
}
