/*
	Prints the text Hello World to the console window.
*/
package cis170.tarun.packagedemo;

// Class name must match file name
public class HelloWorld {

	/*
		- Every java program requires a main method
		- Required for the execution of your java program which begin in main
	*/
    public static void main(String[] args) {
		// Prints the input text to the console
        System.out.println("Hello World!");
    }
}
